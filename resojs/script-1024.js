$(document).ready(function() {
	$('#one').hide();
	$('#two').hide();
	$('#three').hide();
	$('#four').hide();
	$('#five').hide();
	$('#six').hide();
	$('#seven').hide();
	$('#eight').hide();
	$('#nine').hide();

	walkingStick();
	moveStick();
	setTimeout(function(){
		$('#defaultone').hide()
	},970);
	setTimeout(function(){
		$('#nine').show()
	},5500);
});

function walkingStick(){
	setTimeout(function(){
		$('#one').show()
	},1000);

	setTimeout(function(){
		$('#one').hide()
	},1299);

	setTimeout(function(){
		$('#two').show()
	},1300);

	setTimeout(function(){
		$('#two').hide()
	},1599);

	setTimeout(function(){
		$('#three').show()
	},1600);
	
	setTimeout(function(){
		$('#three').hide()
	},1899);

	setTimeout(function(){
		$('#four').show()
	},1900);

	setTimeout(function(){
		$('#four').hide()
	},2199);
	
	setTimeout(function(){
		$('#five').show()
	},2200);

	setTimeout(function(){
		$('#five').hide()
	},2499);
	
	setTimeout(function(){
		$('#six').show()
	},2500);

	setTimeout(function(){
		$('#six').hide()
	},2799);
	
	setTimeout(function(){
		$('#seven').show()
	},2800);

	setTimeout(function(){
		$('#seven').hide()
	},3099);
	
	setTimeout(function(){
		$('#eight').show()
	},3100);

	setTimeout(function(){
		$('#eight').hide()
	},3101);
};
var count = 0;
var repeatLoop = setInterval(function(){
	count++
	if(count === 1){
		clearInterval(repeatLoop);}
	walkingStick()
},2250);

function moveStick(){
	$('img').animate({left:'+=2.6px'},16);
};

var repeatWalk = setInterval(function(){
	moveStick()
},5);
