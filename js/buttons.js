/************************************************
 * THREE BIG BUTTONS
 ***********************************************/
//First button pressed
$('.buttons .button1 a').hover(function() {
    $('#section2 .content2, #section2 .content3').fadeOut(800);
    $('#section2 .content1').fadeIn(800);
    })

//Second button pressed
$('.buttons .button2 a').hover(function() {
    $('#section2 .content2').fadeIn(800);
    $('#section2 .content1, #section2 .content3').fadeOut(800);
    })

//Third button pressed
$('.buttons .button3 a').hover(function() {
    $('#section2 .content1, #section2 .content2').fadeOut(800);
    $('#section2 .content3').fadeIn(800);
    })