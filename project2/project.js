/************************************************
 * THREE BIG BUTTONS
 ***********************************************/
//First button pressed
jQuery('.buttons .button1 a').hover(function() {
    jQuery('#section2 .content2, #section2 .content3').fadeOut(800);
    jQuery('#section2 .content1').fadeIn(800);
    })

//Second button pressed
jQuery('.buttons .button2 a').hover(function() {
    jQuery('#section2 .content2').fadeIn(800);
    jQuery('#section2 .content1, #section2 .content3').fadeOut(800);
    })

//Third button pressed
jQuery('.buttons .button3 a').hover(function() {
    jQuery('#section2 .content1, #section2 .content2').fadeOut(800);
    jQuery('#section2 .content3').fadeIn(800);
    })