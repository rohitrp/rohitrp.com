/* 
 * Dot and person generation
 */
function createDot(x_pos, y_pos) {
    var img = $('<img src="img/dot_off.png" class="map_dot">');
    if($.browser.msie)
        img.css('left', x_pos-16);
    else
        img.css('left', x_pos+138);
    img.css('top', y_pos-16);
    return img;
}

function createPerson(name, position, flag, dots) {
    flag = 'img/' + flag + '.png';
    var holder = $("<a href='index.html'><span class='name'>"+ name +"</span><span class='job'>"+ position +"</span><img src='"+ flag +"' /></a>");

    var mouseOver = function() {
        $.each(dots, function(index, elm) { 
            elm.attr('src', 'img/dot_on.png');
        });
    };

    var mouseOut = function() {
        $.each(dots, function(index, elm) { 
            elm.attr('src', 'img/dot_off.png');
        });
    };

    holder.hover(mouseOver, mouseOut);
    holder.click(function() { return false });
    return holder;
}


/*
 * Map generation
 */
MapSpecs = {
    dayImage: null,
    nightImage: null,
    mapWidth: 891,
    mapHeight: 345
}


function adjustParameters() {
	// How many degrees in one pixel?
	pixelDegW = 360 / MapSpecs.mapWidth;
	pixelDegH = 180 / MapSpecs.mapHeight;

	// Offset from 180 deg. of the most left longitude on the map grid
	// in degrees
	edgeOffset = 9.5;

	// Map grid origin
	centerDegW = (MapSpecs.mapWidth / 2) * pixelDegW - edgeOffset;
	centerDegH = (MapSpecs.mapHeight / 2) * pixelDegH;
}

// Pixel coordinates
function pixelX(deg) {
	var offset = (deg < centerDegW)
		? (centerDegW - deg)
		: (360 - deg + centerDegW);

	return offset / pixelDegW; // in 360 deg. space
}

function pixelY(deg) {
	return (centerDegH - deg) / pixelDegH;
}

// Pixel latitude and longitude
function pixelLambda(x) {
	var deg = x * pixelDegW;
	return (deg < centerDegW)
		? (centerDegW - deg)
		: (360 - deg + centerDegW); // in 360 deg. space
}

function pixelPhi(y, lambda) {
	return centerDegH - y * pixelDegH;
}

function drawDayNightMap(dayImage, nightImage) {
    var map = $('#canvas_map').get(0);

	map.width = MapSpecs.mapWidth;
	map.height = MapSpecs.mapHeight;
	var ctx = map.getContext("2d");	

	ctx.drawImage(dayImage, 0, 0);

	performCalculations(new Date());

	var northSun = DECsun >= 0;
	var startFrom = northSun? 0: (MapSpecs.mapHeight - 1);
	var pstop = function (y) { 
        return northSun? (y < MapSpecs.mapHeight): (y >= 0); 
    };
	var inc = northSun? 1: -1;
	
	ctx.fillStyle = "rgba(0, 0, 0, 0.1)";

	for (var x = 0; x < MapSpecs.mapWidth; ++x)
		for (var y = startFrom; pstop(y); y += inc) {			
			var lambda = pixelLambda(x);
			var phi = pixelPhi(y) + 0.5 * (northSun? -1: 1);

			var centralAngle = sind(phi) * sind(DECsun) 
							 + cosd(phi) * cosd(DECsun) * cosd(GHAsun - lambda);
			centralAngle = Math.acos(centralAngle);
			 
			if (centralAngle > Math.PI / 2) {                                              
				var rectTop = northSun? y: 0;
				var rectHeight = northSun? MapSpecs.mapHeight - rectTop: y + 1;

                ctx.drawImage(nightImage, x, rectTop, 1, rectHeight,
                                          x, rectTop, 1, rectHeight);
                
				//ctx.fillRect(x, rectTop, 1, rectHeight);                                             				
				break;
			}    
		}
}

function loadedImages() {
    if(MapSpecs.dayImage && MapSpecs.nightImage) {
        drawDayNightMap(MapSpecs.dayImage, MapSpecs.nightImage);
    }
}

function initMap() {
    if($('#canvas_map').length == 0)
        return;

	adjustParameters();

    var night_map = new Image();
    night_map.onload = function() {
        MapSpecs.nightImage = night_map;
        loadedImages();
    }
    night_map.src = "img/night.png";

	var day_map = new Image(); 
	day_map.onload = function() {
        MapSpecs.dayImage = day_map;
        loadedImages();
    }
	day_map.src = "img/day.png";
}
